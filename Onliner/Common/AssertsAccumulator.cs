﻿using NUnit.Framework;
using System;
using System.Text;

namespace Onliner.Common
{
    public class AssertsAccumulator
    {
        private StringBuilder _errors { get; set; }
        private bool _assertsPassed { get; set; }

        private String _accumulatedErrorMessage
        {
            get
            {
                return _errors.ToString();
            }
        }

        public AssertsAccumulator()
        {
            _errors = new StringBuilder();
            _assertsPassed = true;
        }

        private void RegisterError(string exceptionMessage)
        {
            _assertsPassed = false;
            _errors.AppendLine(exceptionMessage);
        }

        public void Accumulate(Action assert)
        {
            try
            {
                assert.Invoke();
            }
            catch (Exception exception)
            {
                RegisterError(exception.Message);
            }
        }

        public void Release()
        {
            if (!_assertsPassed)
            {
                throw new AssertionException(_accumulatedErrorMessage);
            }
        }
    }
}
