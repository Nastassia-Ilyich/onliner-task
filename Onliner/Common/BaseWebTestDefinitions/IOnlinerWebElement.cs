﻿using OpenQA.Selenium;

namespace Onliner.Common.BaseWebTestDefinitions
{
    public interface IOnlinerWebElement : IWebElement, IWrapsElement
    {
        By Selector { get; }
    }
}
