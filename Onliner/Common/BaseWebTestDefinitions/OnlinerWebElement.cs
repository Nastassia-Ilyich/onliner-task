﻿using OpenQA.Selenium;
using System.Collections.ObjectModel;
using System.Drawing;
using Onliner.Common.Drivers;
using System;

namespace Onliner.Common.BaseWebTestDefinitions
{
    public class OnlinerWebElement : IOnlinerWebElement
    {
        private readonly By _by;
        private IWebElement _webElement;

        private IWebElement WebElement
        {
            get
            {
                IWebElement result;

                if (_webElement == null)
                {
                    result = GetElement(_by);
                }
                else
                {
                    result = _webElement;
                }
                return result;
            }
        }

        protected static IWebDriver Driver => OnlinerDriver.Browser;
        public IWebElement WrappedElement => WebElement;

        public OnlinerWebElement(By by)
        {
            _by = by;
        }

        public IWebElement GetIWebElement() => WebElement;

        public void ScrollToElement() =>
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", WebElement);

        public IWebElement FindElement(By by)
        {
            return GetElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            try
            {
                Driver.GetWait().Until(x => Driver.FindElements(by).Count > 0);
                return Driver.FindElements(by);
            }
            catch (StaleElementReferenceException)
            {
                return Driver.FindElements(by);
            }
            catch (WebDriverTimeoutException exception)
            {
                throw new WebDriverTimeoutException($"{exception.Message}\nLocator was: {by}", exception.InnerException ?? exception);
            }
        }

        public By Selector => _by;

        public string TagName => WebElement.TagName;

        public string Text => WebElement.Text;

        public bool Enabled => WebElement.Enabled;

        public bool Selected => WebElement.Selected;

        public Point Location => WebElement.Location;

        public Size Size => WebElement.Size;

        public bool Displayed => WebElement.Displayed;

        public bool IsDisplayed()
        {
            try
            {
                WaitForElementIsDisplayed();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsClickable()
        {
            try
            {
                WaitForElementIsClickable();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Clear()
        {
            WebElement.Clear();
        }

        public void Click()
        {
            WebElement.Click();
        }

        public string GetAttribute(string attributeName)
        {
            return WebElement.GetAttribute(attributeName);
        }

        public string GetCssValue(string propertyName)
        {
            return WebElement.GetCssValue(propertyName);
        }

        public string GetDomAttribute(string attributeName)
        {
            return WebElement.GetDomAttribute(attributeName);
        }

        public string GetDomProperty(string propertyName)
        {
            return WebElement.GetDomProperty(propertyName);
        }

        public string GetProperty(string propertyName)
        {
            return WebElement.GetProperty(propertyName);
        }

        public ISearchContext GetShadowRoot()
        {
            return WebElement.GetShadowRoot();
        }

        public void SendKeys(string text)
        {
            WebElement.SendKeys(text);
        }

        public void Submit()
        {
            WebElement.Submit();
        }

        protected IWebElement? GetElement(By by)
        {
            IWebElement element;

            try
            {
                Driver.GetWait().Until(x => Driver.FindElements(by).Count > 0);
                element = Driver.FindElement(by);
            }
            catch (StaleElementReferenceException)
            {
                element = Driver.FindElement(by);
            }
            catch (WebDriverTimeoutException exception)
            {
                return null;
            }
            return element;
        }

        public void WaitForElementIsDisplayed(int? timeout = null) => Driver
            .GetWait()
            .Until(drv => drv.FindElement(_by).Displayed);
        public void WaitForElementIsClickable(int? timeout = null) => Driver
               .GetWait()
               .Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(_by));
    }
}
