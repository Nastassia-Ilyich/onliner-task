﻿using Onliner.Constants;
using Onliner.Enums;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;

namespace Onliner.Common.Drivers
{
    public static class OnlinerDriver
    {
        private static IWebDriver? browser;

        public static IWebDriver Browser
        {
            get
            {
                if (browser == null)
                {
                    throw new NullReferenceException("The WebDriver browser instance was not initialized.");
                }
                return browser;
            }
            private set => browser = value;
        }

        public static void StartBrowser()
        {
            Enum.TryParse(TestSettings.Browser, out Browsers browser);
            switch (browser)
            {
                case Browsers.FIREFOX:
                    Browser = new FirefoxDriver();
                    break;
                case Browsers.CHROME:
                    Browser = new ChromeDriver();
                    break;
                default:
                    throw new ArgumentException("You need to set a valid browser type.");
            }
        }

        public static void StopBrowser()
        {
            Browser?.Quit();
            Browser?.Dispose();
        }

        public static WebDriverWait GetWait(
           this IWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(TestSettings.Timeout));
            return wait;
        }

        public static void OpenTargetUrl(string url) => Browser.Navigate().GoToUrl(url);

    }
}
