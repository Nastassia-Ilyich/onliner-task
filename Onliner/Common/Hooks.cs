﻿using Onliner.Common.Drivers;
using Onliner.Constants;
using System;
using TechTalk.SpecFlow;

namespace Onliner.Common
{
    [Binding]
    public sealed class Hooks
    {

        [BeforeTestRun]
        public static void BeforeFeature()
        {
            OnlinerDriver.StartBrowser();
            OnlinerDriver.Browser.Manage().Window.Maximize();
            OnlinerDriver.Browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(TestSettings.Timeout);
        }

        [AfterTestRun]
        public static void AfterFeature()
        {
            OnlinerDriver.StopBrowser();
        }
    }
}