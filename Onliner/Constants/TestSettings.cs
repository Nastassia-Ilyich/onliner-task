﻿using Microsoft.Extensions.Configuration;

namespace Onliner.Constants
{
    public static class TestSettings
    {
        static TestSettings()
        {
            SetDefaultValues();
        }

        public static IConfiguration TestConfiguration { get; } = new ConfigurationBuilder().AddJsonFile(@"Resources\testsettings.json").Build();
        public static string Browser { get; set; }
        public static int Timeout { get; set; }
        public static string Url { get; set; }
        public static string UserName { get; set; }
        public static string UserPassword { get; set; }
        public static string MainCategoryName { get; set; }
        public static string SubCategoryName { get; set; }
        public static string SectionName { get; set; }
        public static string[] CatalogueSectionsNames { get; set; }
        public static string SearchWord { get; set; }
        public static string ProductGenerationId { get; set; }
        public static string CartUrl { get; set; }

        public static void SetDefaultValues()
        {
            Browser = TestConfiguration["Settings:Common:Browser"];
            Timeout = TryParseIntValue(TestConfiguration["Settings:Common:Timeout"]);
            Url = TestConfiguration["Settings:Common:Url"];
            UserName = TestConfiguration["Settings:Common:UserName"];
            UserPassword = TestConfiguration["Settings:Common:UserPassword"];
            MainCategoryName = TestConfiguration["Catalogue:TVs:MainCategoryName"];
            SubCategoryName = TestConfiguration["Catalogue:TVs:SubCategoryName"];
            SectionName = TestConfiguration["Catalogue:TVs:SectionName"];
            CatalogueSectionsNames = TestConfiguration.GetSection("Catalogue:CatalogueSectionsNames").Get<string[]>();
            SearchWord = TestConfiguration["Search:SearchWord"];
            ProductGenerationId = TestConfiguration["Search:ProductGenerationId"];
            CartUrl = TestConfiguration["Cart:Url"];
        }

        private static int TryParseIntValue(string valueFromTestSettings)
        {
            _ = int.TryParse(valueFromTestSettings, out var value);

            return value;
        }
    }
}
