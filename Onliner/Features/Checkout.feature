﻿Feature: Checkout

As a User, 
I want be able to buy product that I chose

Scenario: Check that user is able to checkout
	Given I am already registered on the site
	And I have navigated to the site
	When I click on Catalogue button
	And I choose TVs section
	And I click on the first TV set
	And I click on sellers' offers
	And I choose seller and add product to basket
	And I go to cart
	And I click checkout
	Then I am redirected to the checkout page

