﻿Feature: CompareTwoItems

As a User, 
I want to be able to see the differences between the products that I want to buy

Scenario: Check that user is able to see differing product characteristics
	Given I have navigated to the site
	When I click on Catalogue button
	And I choose TVs section
	And I click on the first TV set
	And I click on a checkbox that adds TV set to the comparison
	And I return to TVs catalogue
	And I click on the second TV set
	And I click on a checkbox that adds TV set to the comparison
	And I click on a pop-up two items in comparison
	Then I can see differing product characteristics which are highlited in orange
