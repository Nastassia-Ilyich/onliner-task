﻿Feature: LoginWithValidCredits

As a User, 
I want to be able to login with the credentials that were created via Register form

Scenario: Check that user is able to login
	Given I am already registered on the site
	When I open site
	And I click button Войти
	And Enter user's e-mail and password
	And Click button Войти
	Then I check that I'm redirected to the main page

