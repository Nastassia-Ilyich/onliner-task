﻿Feature: MenuNavigation

As a User, 
I want to be able to see all sections of the catalog by clicking on one button

Scenario: Check that all sections of catalog are available to be chosen by clicking on Catalogue button
	Given I have navigated to the site
	When I click on Catalogue button
	Then I am redirected to the page where the catalog sections are listed 
