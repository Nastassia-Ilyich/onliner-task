﻿Feature: ProductSearch

As a User, 
I want to be able to use the search to find goods that are interesting for me 

Scenario Outline: Check that user is able to get search results
	Given I have navigated to the site
	When I type '<SearchTerm>' into the search bar
	Then I see cards with the name and description of the product, containg '<SearchTerm>' or Search Term with '<Id>' in the name

Examples:
	| SearchTerm   | Id |
	| Thinkpad T14 | s  |