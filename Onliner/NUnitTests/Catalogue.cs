﻿using NUnit.Framework;
using Onliner.Pages;

namespace Onliner.NUnitTests
{
    public class Catalogue
    {
        private MainPage _mainPage = new MainPage();
        private CataloguePage _cataloguePage = new CataloguePage();

        [Test]
        public void CheckExistenceOfCatalogSections()
        {
            _mainPage.OpenMainPage();
            _mainPage.ClickOnCatalogueButton();
            Assert.IsTrue(_cataloguePage.CheckThatCatalogueSectionsAvailable(), "Catalog does not all the listed sections");
        }
    }
}
