﻿using NUnit.Framework;
using Onliner.Common.Drivers;
using Onliner.Constants;
using Onliner.Pages;

namespace Onliner.NUnitTests
{
    public class Checkout
    {
        private MainPage _mainPage = new MainPage();
        private CataloguePage _cataloguePage = new CataloguePage();
        private LoginPage _loginPage = new LoginPage();
        private ItemPage _itemPage = new ItemPage();
        private CartPage _cartPage = new CartPage();
        private CheckoutPage _checkoutPage = new CheckoutPage();

        [Test]
        public void CheckAvailabilityProceedToChechoutPage()
        {
            _loginPage.GetUserCredentials();
            _mainPage.OpenMainPage();
            _mainPage.ClickOnCatalogueButton();
            _cataloguePage.ChooseSectionFromCatalogue(TestSettings.MainCategoryName, TestSettings.SubCategoryName, TestSettings.SectionName);
            _cataloguePage.ClickOnTheFirstItemInCatalogue();
            _itemPage.ProceedToOffers();
            _itemPage.AddProductToTheBasket();
            _itemPage.GoToBasket();
            _itemPage.ProductsAddedToBasket.ForEach(item => Assert.That(_cartPage.GetProductTitlesInTheCart().Contains(item), item + " is not added to the basket"));
            _cartPage.GoToCheckout();
            Assert.IsTrue(_checkoutPage.ConfirmCheckoutPageOpened(), "Checkout page is not opened");
            OnlinerDriver.OpenTargetUrl(TestSettings.CartUrl);
            Assert.IsTrue(_cartPage.DeleteItemFromCart(), "Item is not deleted from cart");
        }
    }
}