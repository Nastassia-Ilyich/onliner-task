﻿using NUnit.Framework;
using Onliner.Common;
using Onliner.Constants;
using Onliner.Pages;

namespace Onliner.NUnitTests
{
    public class Compare
    {
        private MainPage _mainPage = new MainPage();
        private CataloguePage _cataloguePage = new CataloguePage();
        private ItemPage _itemPage = new ItemPage();
        private ComparePage _comparePage = new ComparePage();

        [Test]
        public void CheckItemsAreCompared()
        {
            _mainPage.OpenMainPage();
            _mainPage.ClickOnCatalogueButton();
            _cataloguePage.ChooseSectionFromCatalogue(TestSettings.MainCategoryName, TestSettings.SubCategoryName, TestSettings.SectionName);
            _cataloguePage.ClickOnTheFirstItemInCatalogue();
            _itemPage.AddItemToTheComparison();
            BasePage.NavigateTothePreviousPage();
            _cataloguePage.ClickOnTheSecondItemInCatalogue();
            _itemPage.AddItemToTheComparison();
            _itemPage.ClickOnCompareButton();
            _itemPage.ItemsTitlesForComparison.ForEach(item => Assert.That(_comparePage.GetItemNamesFromComparisonPage().Contains(item), item + " item is not added to comparison"));
            Assert.IsTrue(_comparePage.ConfirmDifferingCharacteristicsAreHighlighted(), "Differing characteristics are not highlighted in orange");
            _comparePage.ClearComparison();
        }
    }
}
