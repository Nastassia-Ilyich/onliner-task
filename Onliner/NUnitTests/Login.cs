﻿using NUnit.Framework;
using Onliner.Common;
using Onliner.Common.Drivers;
using Onliner.Constants;
using Onliner.Pages;

namespace Onliner.NUnitTests
{
    public class LoginPageTest
    {
        private MainPage _mainPage = new MainPage();
        private LoginPage _loginPage = new LoginPage();

        [Test]
        public void LoginWithValidCredentials()
        {
            _loginPage.GetUserCredentials();
            _mainPage.OpenMainPage();
            _mainPage.ClickEnterButton();
            _loginPage.EnterUserCredentials();
            _loginPage.Login();
            BasePage.WaitTillPageIsLoaded();
            Assert.IsTrue(TestSettings.Url.Equals(OnlinerDriver.Browser.Url), "User is not redirected to the main page");
        }
    }
}
