﻿using NUnit.Framework;
using Onliner.Common;
using Onliner.Pages;
using System.Collections.Generic;
using System.Linq;

namespace Onliner.NUnitTests
{
    public class MainPageTest
    {
        private MainPage _mainPage = new MainPage();

        [Test]
        public void ComplianceOfProductNamesWithSpecifiedSearchTerm()
        {
            var _assertsAccumulator = new AssertsAccumulator();
            var _searchTerm = _mainPage.GetSearchTestData().Keys.First();
            var _id = _mainPage.GetSearchTestData().Values.First();
            _mainPage.OpenMainPage();
            _mainPage.GetSearchTestData();
            _mainPage.EnterSearchTerm(_searchTerm);
            List<string> _productNames = _mainPage.GetAllProductNamesFromSearchResults();
            foreach (string _productName in _productNames)
            {
                _assertsAccumulator.Accumulate(() =>
               Assert.That(_productName.ToLower().Contains(_searchTerm.ToLower() + " ") || _productName.ToLower().Contains(_searchTerm.ToLower() + _id + " "),
               "Product item does not contain " + _searchTerm + " or " + _searchTerm + _id));
            }
            _assertsAccumulator.Release();
        }
    }
}
