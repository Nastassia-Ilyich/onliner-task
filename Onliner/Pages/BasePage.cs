﻿using Onliner.Common.Drivers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace Onliner.Common
{
    public class BasePage
    {
        private static TimeSpan _defaultTimespan = new TimeSpan(0, 0, 15);

        public BasePage()
        {
            WaitTillPageIsLoaded();
        }

        public static void WaitTillPageIsLoaded() =>
            new WebDriverWait(OnlinerDriver.Browser, _defaultTimespan)
                .Until(_driver => ((IJavaScriptExecutor)_driver)
                .ExecuteScript("return document.readyState")
                .Equals("complete"));

        public static void WaitTillElementExists(By locator) =>
            new WebDriverWait(OnlinerDriver.Browser, _defaultTimespan).Until(ExpectedConditions.ElementExists(locator));

        public static void NavigateTothePreviousPage()
        {
            OnlinerDriver.Browser.Navigate().Back();
            WaitTillPageIsLoaded();
        }

        public static void WaitForElementIsVisible(By locator) =>
            new WebDriverWait(OnlinerDriver.Browser, _defaultTimespan).Until(ExpectedConditions.ElementIsVisible(locator));

        public void ForceClickUsingJS(IWebElement element) =>
          ((IJavaScriptExecutor)OnlinerDriver.Browser).ExecuteScript("arguments[0].click({ force: true });", element);
    }
}
