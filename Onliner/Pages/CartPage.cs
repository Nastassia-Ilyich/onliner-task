﻿using Onliner.Common;
using Onliner.Common.BaseWebTestDefinitions;
using Onliner.Common.Drivers;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace Onliner.Pages
{
    public class CartPage : BasePage
    {
        private OnlinerWebElement _proceedToCheckout { get; } = new OnlinerWebElement(By.XPath("//a[contains(text(),'Перейти к оформлению')]"));
        private OnlinerWebElement _removedItemText { get; } = new OnlinerWebElement(By.XPath("//div[contains(text(),'Вы удалили')]"));
        private IList<IWebElement> _productsInTheCart => OnlinerDriver.Browser.FindElements(By.CssSelector(".cart-form__offers-part_data .cart-form__link_base-alter"));
        private OnlinerWebElement _removeProductButton { get; } = new OnlinerWebElement(By.CssSelector(".cart-form__button_remove"));

        public void GoToCheckout()
        {
            _proceedToCheckout.Click();
        }

        public HashSet<string> GetProductTitlesInTheCart()
        {
            HashSet<string> _productTitles = new HashSet<string>();
            foreach (var item in _productsInTheCart)
            {
                _productTitles.Add(item.Text);
            }
            return _productTitles;
        }

        public bool DeleteItemFromCart()
        {
            //Use force click cause mouseover, action class don't work
            ForceClickUsingJS(_removeProductButton);
            return _removedItemText.Displayed;
        }
    }
}
