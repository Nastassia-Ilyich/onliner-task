﻿using Onliner.Common;
using Onliner.Common.BaseWebTestDefinitions;
using Onliner.Common.Drivers;
using Onliner.Constants;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
namespace Onliner.Pages
{
    public class CataloguePage : BasePage
    {
        private IList<IWebElement> _catalogueSections => OnlinerDriver.Browser.FindElements(By.CssSelector(".catalog-navigation_opened > ul > li.catalog-navigation-classifier__item"));
        private OnlinerWebElement _firstProductInCatalogue => new OnlinerWebElement(By.CssSelector("div:nth-of-type(2) > .schema-product .schema-product__part > .schema-product__title > a > span"));
        private OnlinerWebElement _secondProductInCatalogue => new OnlinerWebElement(By.CssSelector("div:nth-of-type(3) > .schema-product .schema-product__part > .schema-product__title > a > span"));

        public bool CheckThatCatalogueSectionsAvailable()
        {
            List<string> _necessaryCatalogues = new List<string>(TestSettings.CatalogueSectionsNames);
            List<string> _catalogueSectionNames = new List<string>();

            foreach (var _catalogueSection in _catalogueSections)
            {
                _catalogueSectionNames.Add(_catalogueSection.Text);
            }

            IEnumerable<string> difInWebList = _catalogueSectionNames.Except(_necessaryCatalogues);
            IEnumerable<string> difInTestDataList = _necessaryCatalogues.Except(_catalogueSectionNames);
            return !difInWebList.Any() && !difInTestDataList.Any();
        }

        public void ChooseSectionFromCatalogue(string mainCategoryName, string subCategoryName, string sectionName)
        {
            new OnlinerWebElement(By.XPath("//span[contains(text(),'"+ mainCategoryName + "')]")).Click();
            new OnlinerWebElement(By.XPath("//div[contains(text(),'" + subCategoryName + "')]")).Click();
            new OnlinerWebElement(By.XPath("//span[contains(@class,'title') and contains(text(),'" + sectionName + "')]")).Click();
        }

        public void ClickOnTheFirstItemInCatalogue()
        {
            _firstProductInCatalogue.Click();
        }

        public void ClickOnTheSecondItemInCatalogue()
        {
            _secondProductInCatalogue.Click();
        }
    }
}
