﻿using Onliner.Common;
using Onliner.Common.BaseWebTestDefinitions;
using OpenQA.Selenium;

namespace Onliner.Pages
{
    public class CheckoutPage : BasePage
    {
        private OnlinerWebElement _proceedToPaymentButton => new OnlinerWebElement(By.CssSelector(".cart-form__button_responsive"));

        public bool ConfirmCheckoutPageOpened()
        {
            return _proceedToPaymentButton.Displayed;
        }
    }
}
