﻿using NUnit.Framework;
using Onliner.Common;
using Onliner.Common.BaseWebTestDefinitions;
using Onliner.Common.Drivers;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace Onliner.Pages
{
    public class ComparePage : BasePage
    {
        private IList<IWebElement> _differentCharacteristics => OnlinerDriver.Browser.FindElements(By.CssSelector(".product-table__cell_accent"));
        private IList<IWebElement> _itemsTitlesElements => OnlinerDriver.Browser.FindElements(By.CssSelector(".product-summary__caption"));
        private OnlinerWebElement _clearComparison => new OnlinerWebElement(By.CssSelector(".catalog-masthead__aside > .button.button_gray.button_small.product-table__clear"));
       
        public HashSet<string> GetItemNamesFromComparisonPage()
        {
            HashSet<string> _productTitles = new HashSet<string>();
            foreach (var item in _itemsTitlesElements)
            {
                _productTitles.Add(item.Text);
            }
            return _productTitles;
        }
        
        public bool ConfirmDifferingCharacteristicsAreHighlighted()
        {
            int _unlitItems = 0;
            foreach (var element in _differentCharacteristics)
            {
                if(!element.GetCssValue("background").Contains("rgb(255, 236, 196"))
                {
                    _unlitItems++;
                };   
            }
            return _unlitItems == 0;
        }
       
        public void ClearComparison()
        {
            _clearComparison.Click();
        }
    }
}
