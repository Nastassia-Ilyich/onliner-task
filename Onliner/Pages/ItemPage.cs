﻿using Onliner.Common;
using Onliner.Common.BaseWebTestDefinitions;
using Onliner.Common.Drivers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace Onliner.Pages
{
    public class ItemPage : BasePage
    {
        private OnlinerWebElement _addToComparisonCheckbox => new OnlinerWebElement(By.CssSelector("li#product-compare-control .i-checkbox__faux"));
        private OnlinerWebElement _compareButton => new OnlinerWebElement(By.CssSelector(".compare-button__sub.compare-button__sub_main"));
        private OnlinerWebElement _itemTitle => new OnlinerWebElement(By.CssSelector(".catalog-masthead__title"));
        private OnlinerWebElement _offersButton => new OnlinerWebElement(By.CssSelector(".button.button_orange.button_big.offers-description__button"));
        private OnlinerWebElement _goToBasketFromSideBar => new OnlinerWebElement(By.XPath("//a[contains(text(),'Перейти в корзину')]"));
        private OnlinerWebElement _goToBasketFromOffers => new OnlinerWebElement(By.CssSelector(".helpers_hide_tablet>.button-style_another"));
        private IList<IWebElement> _addToBasket => OnlinerDriver.Browser.FindElements(By.CssSelector(".helpers_hide_tablet > .offers-list__button.offers-list__button_cart"));
        private OnlinerWebElement _acceptLocationPopupButton => new OnlinerWebElement(By.CssSelector("span.button-style.button-style_another.button-style_base.offers-form__button"));
        public List<string> ItemsTitlesForComparison { get => itemsTitlesForComparison; set => itemsTitlesForComparison = value; }
        private List<string> itemsTitlesForComparison = new List<string>();
        public List<string> ProductsAddedToBasket { get => productsAddedToBasket; set => productsAddedToBasket = value; }
        private List<string> productsAddedToBasket = new List<string>();

        public void AddItemToTheComparison()
        {
            itemsTitlesForComparison.Add(_itemTitle.Text);
            _addToComparisonCheckbox.Click();
        }

        public void ClickOnCompareButton()
        {
            _compareButton.Click();
        }

        public void ProceedToOffers()
        {
            productsAddedToBasket.Add(_itemTitle.Text);
            _offersButton.Click();
        }

        public void AddProductToTheBasket()
        {
            Random random = new Random();
            var randomSeller = random.Next(_addToBasket.Count);
            try
            {
                _addToBasket[randomSeller].Click();
            }
            catch (ElementClickInterceptedException)
            {
                _acceptLocationPopupButton.Click();
                _addToBasket[randomSeller].Click();
            }
        }

        public void GoToBasket()
        {
            if (_acceptLocationPopupButton.IsDisplayed() || _goToBasketFromSideBar.IsDisplayed())
            {
                if (_acceptLocationPopupButton.IsClickable())
                {
                    _acceptLocationPopupButton.Click();
                }
                if (_goToBasketFromSideBar.IsClickable())
                {
                    _goToBasketFromSideBar.Click();
                }
                if (_goToBasketFromOffers.IsClickable())
                {
                    _goToBasketFromOffers.Click();
                }
            }
            else
            {
                _goToBasketFromOffers.Click();
            }
        }
    }
}
