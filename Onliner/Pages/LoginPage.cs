﻿using Onliner.Common;
using Onliner.Common.BaseWebTestDefinitions;
using Onliner.Common.Drivers;
using Onliner.Constants;
using OpenQA.Selenium;

namespace Onliner.Pages
{
    public class LoginPage : BasePage
    {
        private OnlinerWebElement EmailField => new OnlinerWebElement(By.CssSelector("input[type=text].auth-input"));
        private OnlinerWebElement PasswordField => new OnlinerWebElement(By.CssSelector("input[type=password].auth-input"));
        private OnlinerWebElement LoginFormEnterButton => new OnlinerWebElement(By.CssSelector("button[type=submit].auth-form__button"));
        private string _userName;
        private string _password;

        public void GetUserCredentials()
        {
            _userName = TestSettings.UserName;
            _password = TestSettings.UserPassword;
        }

        public void EnterUserCredentials()
        {
            EmailField.SendKeys(_userName);
            PasswordField.SendKeys(_password);
        }

        public void Login()
        {
            LoginFormEnterButton.Click();
        }
    }
}
