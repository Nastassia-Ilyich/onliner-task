﻿using Onliner.Common;
using Onliner.Common.BaseWebTestDefinitions;
using Onliner.Common.Drivers;
using Onliner.Constants;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;

namespace Onliner.Pages
{
    public class MainPage : BasePage
    {
        private OnlinerWebElement SearchField => new OnlinerWebElement(By.CssSelector("input[name='query']"));
        private OnlinerWebElement ModalIframe => new OnlinerWebElement(By.CssSelector(".modal-iframe"));
        private OnlinerWebElement SearchClose => new OnlinerWebElement(By.CssSelector(".search__close"));
        private IList<IWebElement> _productNames => OnlinerDriver.Browser.FindElements(By.CssSelector(".product__title>a[target='_parent']"));
        private OnlinerWebElement AuthEnterButton => new OnlinerWebElement(By.CssSelector(".auth-bar__item--text"));
        private OnlinerWebElement CatalogueButton => new OnlinerWebElement(By.CssSelector(" li:nth-child(1) > a.b-main-navigation__link > span"));
        private By SearchContentWrapper => By.CssSelector(".search__content-wrapper");

        public Dictionary<string, string> GetSearchTestData()
        {
            Dictionary<string, string> _searchDataDictionary = new Dictionary<string, string>();
            _searchDataDictionary.Add(TestSettings.SearchWord, TestSettings.ProductGenerationId);
            return _searchDataDictionary;
        }

        public void OpenMainPage()
        {
            OnlinerDriver.Browser.Navigate().GoToUrl(TestSettings.Url);
        }

        public void ClickEnterButton()
        {
            AuthEnterButton.Click();
        }

        public void EnterSearchTerm(string searchTerm)
        {
            SearchField.SendKeys(searchTerm);
        }

        public List<string> GetAllProductNamesFromSearchResults()
        {
            Actions actions = new Actions(OnlinerDriver.Browser);
            var productNames = new List<string>();
            OnlinerDriver.Browser.SwitchTo().DefaultContent();
            OnlinerDriver.Browser.SwitchTo().Frame(ModalIframe);
            WaitTillElementExists(SearchContentWrapper);
            while (true)
            {
                int productsCountOld = _productNames.Count;
                actions.MoveToElement(_productNames[productsCountOld - 1]).MoveByOffset(10, 30).Build().Perform();
                WaitForElementIsVisible(By.CssSelector("li:nth-of-type(" + (productsCountOld - 1) + ") > .result__wrapper > .result__item.result__item_product"));
                int productsCountNew = _productNames.Count;
                if (productsCountNew == productsCountOld)
                {
                    break;
                }
            }
            foreach (var element in _productNames)
            {
                productNames.Add(element.Text);
            }
            SearchClose.Click();
            OnlinerDriver.Browser.SwitchTo().DefaultContent();

            return productNames;
        }

        public void ClickOnCatalogueButton()
        {
            CatalogueButton.Click();
        }
    }
}
