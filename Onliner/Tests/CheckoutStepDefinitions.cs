using NUnit.Framework;
using Onliner.Common.Drivers;
using Onliner.Constants;
using Onliner.Pages;
using TechTalk.SpecFlow;

namespace Onliner
{
    [Binding]
    public class CheckoutStepDefinitions
    {
        private ItemPage _itemPage = new ItemPage();
        private CartPage _cartPage = new CartPage();
        private CheckoutPage _checkoutPage = new CheckoutPage();

        [When(@"I click on sellers' offers")]
        public void WhenIClickOnSellersOffers()
        {
            _itemPage.ProceedToOffers();
        }

        [When(@"I choose seller and add product to basket")]
        public void WhenIChooseSellerAndAddProductToBasket()
        {
            _itemPage.AddProductToTheBasket();
        }

        [When(@"I go to cart")]
        public void WhenIGoToCart()
        {
            _itemPage.GoToBasket();
        }

        [When(@"I click checkout")]
        public void WhenIClickCheckout()
        {
            _itemPage.ProductsAddedToBasket.ForEach(item => Assert.That(_cartPage.GetProductTitlesInTheCart().Contains(item), item + " is not added to the basket"));
            _cartPage.GoToCheckout();
        }

        [Then(@"I am redirected to the checkout page")]
        public void ThenIAmRedirectedToTheCheckoutPage()
        {
            Assert.IsTrue(_checkoutPage.ConfirmCheckoutPageOpened(), "Checkout page is not opened");
            OnlinerDriver.OpenTargetUrl(TestSettings.CartUrl);
            Assert.IsTrue(_cartPage.DeleteItemFromCart(), "Item is not deleted from cart");
        }
    }
}
