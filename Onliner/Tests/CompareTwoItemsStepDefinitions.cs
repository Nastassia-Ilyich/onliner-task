using NUnit.Framework;
using Onliner.Common;
using Onliner.Constants;
using Onliner.Pages;
using TechTalk.SpecFlow;

namespace Onliner
{
    [Binding]
    public class CompareTwoItemsStepDefinitions
    {
        private CataloguePage _cataloguePage = new CataloguePage();
        private ItemPage _itemPage = new ItemPage();
        private ComparePage _comparePage = new ComparePage();

        [When(@"I choose TVs section")]
        public void WhenIChooseTVsSection()
        {
            _cataloguePage.ChooseSectionFromCatalogue(TestSettings.MainCategoryName, TestSettings.SubCategoryName, TestSettings.SectionName);
        }

        [When(@"I click on the first TV set")]
        public void WhenIClickOnTheFirstTVSet()
        {
            _cataloguePage.ClickOnTheFirstItemInCatalogue();
        }

        [When(@"I click on a checkbox that adds TV set to the comparison")]
        public void WhenIClickOnACheckboxThatAddsTVSetToTheComparison()
        {
            _itemPage.AddItemToTheComparison();
        }

        [When(@"I return to TVs catalogue")]
        public void WhenIReturnToTVsCatalogue()
        {
            BasePage.NavigateTothePreviousPage();
        }

        [When(@"I click on the second TV set")]
        public void WhenIClickOnTheSecondTVSet()
        {
            _cataloguePage.ClickOnTheSecondItemInCatalogue();
        }

        [When(@"I click on a pop-up two items in comparison")]
        public void WhenIClickOnAPop_UpItemsInComparison()
        {
            _itemPage.ClickOnCompareButton();
        }

        [Then(@"I can see differing product characteristics which are highlited in orange")]
        public void ThenICanSeeDifferingProductCharacteristicsWhichAreHighlitedInOrange()
        {
            _itemPage.ItemsTitlesForComparison.ForEach(item => Assert.That(_comparePage.GetItemNamesFromComparisonPage().Contains(item), item + " item is not added to comparison"));
            Assert.IsTrue(_comparePage.ConfirmDifferingCharacteristicsAreHighlighted(), "Differing characteristics are not highlighted in orange");
            _comparePage.ClearComparison();
        }
    }
}
