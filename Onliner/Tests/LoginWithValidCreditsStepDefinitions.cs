using NUnit.Framework;
using Onliner.Common;
using Onliner.Common.Drivers;
using Onliner.Constants;
using Onliner.Pages;
using TechTalk.SpecFlow;

namespace Onliner
{
    [Binding]
    public class LoginWithValidCreditsStepDefinitions
    {
        private MainPage _mainPage = new MainPage();
        private LoginPage _loginPage = new LoginPage();

        [Given(@"I am already registered on the site")]
        public void GivenIAmAlreadyRegisteredOnTheSite()
        {
            _loginPage.GetUserCredentials();
        }

        [When(@"I open site")]
        public void WhenIOpenSite()
        {
            _mainPage.OpenMainPage();
        }

        [When(@"I click button Войти")]
        public void WhenIClickButtonВойти()
        {
            _mainPage.ClickEnterButton();
        }

        [When(@"Enter user's e-mail and password")]
        public void WhenEnterUsersE_MailAndPassword()
        {
            _loginPage.EnterUserCredentials();
        }

        [When(@"Click button Войти")]
        public void WhenClickButtonВойти()
        {
            _loginPage.Login();
        }

        [Then(@"I check that I'm redirected to the main page")]
        public void ThenICheckThatImRedirectedToTheMainPage()
        {
            BasePage.WaitTillPageIsLoaded();
            Assert.IsTrue(TestSettings.Url.Equals(OnlinerDriver.Browser.Url), "User is not redirected to the main page");
        }
    }
}
