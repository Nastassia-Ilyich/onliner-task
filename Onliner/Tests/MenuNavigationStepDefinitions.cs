using NUnit.Framework;
using Onliner.Pages;
using TechTalk.SpecFlow;

namespace Onliner
{
    [Binding]
    public class MenuNavigationStepDefinitions
    {
        private MainPage _mainPage = new MainPage();
        private CataloguePage _cataloguePage = new CataloguePage();

        [When(@"I click on Catalogue button")]
        public void WhenIClickOnCatalogueButton()
        {
            _mainPage.ClickOnCatalogueButton();
        }

        [Then(@"I am redirected to the page where the catalog sections are listed")]
        public void ThenIAmRedirectedToThePageWhereTheCatalogSectionsAreListed()
        {
            Assert.IsTrue(_cataloguePage.CheckThatCatalogueSectionsAvailable(), "Catalog does not all the listed sections");
        }
    }
}
