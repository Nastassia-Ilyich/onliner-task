using NUnit.Framework;
using Onliner.Common;
using Onliner.Pages;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace Onliner
{
    [Binding]
    public class ProductSearchStepDefinitions
    {
        private MainPage _mainPage = new MainPage();

        [Given(@"I have navigated to the site")]
        public void GivenIHaveNavigatedToTheSite()
        {
            _mainPage.OpenMainPage();
        }

        [When(@"I type '(.*)' into the search bar")]
        public void WhenITypeThinkpadTIntoTheSearchBar(string _searchTerm)
        {
            _mainPage.EnterSearchTerm(_searchTerm);
        }

        [Then(@"I see cards with the name and description of the product, containg '(.*)' or Search Term with '(.*)' in the name")]
        public void ThenISeeCardsWithTheNameAndDescriptionOfTheProductContaingSeaechTerm(string _searchTerm, string _id)
        {
            var _assertsAccumulator = new AssertsAccumulator();
            IList<string> _productNames = _mainPage.GetAllProductNamesFromSearchResults();
            foreach (string _productName in _productNames)
            {
                _assertsAccumulator.Accumulate(() =>
               Assert.That(_productName.ToLower().Contains(_searchTerm.ToLower() + " ") || _productName.ToLower().Contains(_searchTerm.ToLower() + _id + " "),
               "Product item does not contain " + _searchTerm + " or " + _searchTerm + _id));
            }
            _assertsAccumulator.Release();
        }
    }
}
